module Main (main) where

import Data.List
import Data.Word
import Graphics.Gloss
import qualified Data.ByteString as B

main :: IO ()
main = do
    file <- fmap B.unpack $ B.readFile "LICENSE"
    let dupDigraphs = countDuplicates $ digraphs file
    let med = median $ fmap snd $ dupDigraphs
    putStrLn $ "Using median: " ++ show med
    let pic = translate (-128) (128) $ pictures $ coloredSquares dupDigraphs med
    display (InWindow "Digraph Visualization" (256, 256) (500, 500)) black pic

median :: Ord a => [a] -> a
median as = (sort as) !! (length as `div` 2)

coloredSquares :: [((Word8,Word8),Int)] -> Int -> [Picture]
coloredSquares [a] i = [coloredSquare a i]
coloredSquares (a:as) i = (coloredSquare a i) : coloredSquares as i

coloredSquare :: ((Word8,Word8),Int) -> Int -> Picture
coloredSquare ((x,y),c) i = color (makeColorI j j j 255) $ translate (fromWord8 x) ((-1) * fromWord8 y) $ rectangleSolid 1 1
                            where j = (255 * c) `div` i

countDuplicates :: Ord a => [a] -> [(a,Int)]
countDuplicates a = zip a $ fmap length $ group $ sort a

fromWord8 :: Num a => Word8 -> a
fromWord8 = fromInteger . toInteger

digraphs :: [a] -> [(a,a)]
digraphs a = zip a $ tail a

squares :: [(Word8,Word8)] -> [Picture]
squares [(x,y)] = [translate (fromWord8 x) (fromWord8 y) $ rectangleSolid 1 1]
squares ((x,y):as) = (translate (fromWord8 x) (fromWord8 ((-1) * y)) $ rectangleSolid 1 1) : squares as
